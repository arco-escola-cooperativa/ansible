// IMPORTANT: Start your code on the 2nd line
// https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig

pref("general.config.filename", "firefox.cfg");
pref("general.config.obscure_value", 0);
