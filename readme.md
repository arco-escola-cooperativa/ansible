## Arco Escola Cooperativa
Projeto Ansible para automatizar instalação e configuração das máquinas
da escola

### O que faz
 - Instala softwares padrão (incluindo Chrome)
 - Define senhas e usuários
 - Desligamento automático
 - Chrome e Firefox entram por padrão no modo anônimo

### Requisitos

#### Servidor:
 - ssh
 - python3
 - sudo

#### Cliente:
 - Ansible (> 2.4)
 - nmap
 - ssh
 - sshpass (não necessário se sua chave ssh estiver nas máquinas)

## Como usar

`$ sudo ./collect-ip.sh`

Os ips das máquinas não são estáticas nem acessíveis fora da rede local.
O script `collect-id.sh`, chamado de dentro da rede local, coleciona os
endereços ip corretos, associando-os às respectivas máquinas, e a partir
disso constrói o arquivo inventário (`hosts`). Daí fazemos:

`$ ansible-playbook -k -K aula.yml`

A opção -k não é necessária se o servidor conhece sua chave ssh pública
(recomendado!)


#### Sobre Ansible

As partes mais importantes são:

 - arquivos "inventory":
    lista os servidores e seus endereços

 - diretórios "task":
    o que fazer em cada procedimento; receitas

 - arquivos "playbook":
    quais receitas executar e em qual servidor
