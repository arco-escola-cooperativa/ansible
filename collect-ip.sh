#!/bin/bash

# The command below (requiring root privilege) maps MAC addresses to ip
# addresses.

# sudo nmap -p 22 -sS 192.168.0.0/24

# This script knows the MAC addresses of all the computers. Thus it can
# update the ip addresses of the available servers on the ansible
# inventory.


# MAC addresses:
# Secretaria 1: EC:F4:BB:F4:9C:FB
# Secretaria 2 ("volantes"): A4:BA:DB:06:1C:BC
# Secretaria 3 (upper floor): 00:19:66:F5:B6:C0
# Sala 1: 0C:EE:E6:FC:EA:8B
# Sala 2: D0:27:88:60:F9:16
# Sala 3: 90:FB:A6:E8:FF:EE
# Ateliê: 0C:EE:E6:FC:F7:25
# Laboratório: A4:1F:72:F7:78:30
# Carrinho de projeção: 00:0F:00:C5:59:12 (usually turned off)
# Biblioteca: 00:24:8C:FC:5C:19 (Not connected. ssh not installed)

if [ $EUID -ne 0 ]; then
  echo "Please run this script as root. Consider \`sudo ./collect-ip.sh\`"
  exit 1
fi

host_names=(sec1 sec2 sec3 sala1 sala2 sala3 atelie laboratorio projetor biblioteca)
mac_adds=(
  EC:F4:BB:F4:9C:FB #sec1
  A4:BA:DB:06:1C:BC #sec2
  00:19:66:F5:B6:C0 #sec3
  0C:EE:E6:FC:EA:8B #sala1
  D0:27:88:60:F9:16 #sala2
  90:FB:A6:E8:FF:EE #sala3
  0C:EE:E6:FC:F7:25 #atelie
  A4:1F:72:F7:78:30 #laboratorio
  00:0F:00:C5:59:12 #projetor
  00:24:8C:FC:5C:19 #biblioteca
)


ip=""
mac=""

while read line; do
  echo $line
  t=$(echo "$line" | grep -oP '\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b')
  if [ "$t" ]; then
    ip="$t"

  elif t=$(echo "$line" | grep -oP '\b(\w\w:){5}\w\w\b'); then
    if [ "$t" ]; then

      for i in ${!mac_adds[@]}; do
        if [ "$t" = "${mac_adds[$i]}" ]; then
          ip_adds[$i]="$ip"
          break
        fi
      done
    fi
  fi
done < <(nmap -p 22 -sS 192.168.0.0/24)

# for i in ${!mac_adds[@]}; do
#   echo ${host_names[$i]}: ${ip_adds[$i]} ${mac_adds[$i]}
# done

cp hosts hosts.bak
# use # instead of newlines
data="$(cat hosts | tr '\n' '#')"

for i in ${!host_names[@]}; do
  if [ ${ip_adds[$i]} ]; then
    echo "Found ip from ${host_names[$i]}"
    data="$(echo "$data" | sed -r 's/\['${host_names[$i]}'\]\#([0-9]{1,3}\.){3}[0-9]{1,3}/\['${host_names[$i]}'\]\#'${ip_adds[$i]}'/')"
  fi
done

echo "$data" | tr '#' "\n" > hosts
