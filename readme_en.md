## Arco Escola Cooperativa
Ansible project to automate installation and configuration of the
school's computers

### What it does
 - Install standard software (including Google Chrome)
 - Sets users and passwords
 - Automatic shutdown
 - Chrome and Firefox by default on anonymous mode

### Dependencies

### Server side:
 - ssh
 - python3
 - sudo

### Client side:
 - Ansible (> 2.4)
 - nmap
 - ssh
 - sshpass (not necessary if the server knows your ssh key)

## How to use

`$ sudo ./collect-ip.sh`

The severs ips' are not static nor accessible outside local network.
The `collect-id.sh` script, called from within the local network,
gathers the correct ip addresses for the severs and builds the
inventory file. Now we can do:

`$ ansible-playbook -k -K aula.yml` or
`$ ansible-playbook -k -K secretaria.yml`

You don't need the -k option if the server knows your ssh key
(recommened!)


#### About ansible

The most important pieces are:

 - inventory files:
    list the servers you have

 - task folders:
    what to do in each procedure; recipes

 - playbook files:
    which roles to execute and in which server
